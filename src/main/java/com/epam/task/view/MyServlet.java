package com.epam.task.view;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=UTF8");

        try(PrintWriter out = resp.getWriter()) {
            out.println("<html>");
            out.println("<head><title>Servlet sample</title></head>");
            out.println("<body>");
            out.println("<p>Запрошенный ресурс: " + req.getRequestURI() + "</p>");
            out.println("<p>Протокол: " + req.getProtocol() + "</p>");
            out.println("<p>Адрес сервера: " + req.getRemoteAddr() + "</p>");
            out.println("</body></html>");
        }
    }
}
