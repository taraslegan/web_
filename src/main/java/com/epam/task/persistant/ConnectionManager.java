package com.epam.task.persistant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static String url = "jdbc:mysql://localhost:3306/autobase?autoReconnect=true&useSSL=false";
    private static String user = "root";
    private static String password = "admin";

    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection == null) initializeConnection();
        return connection;
    }

    public static void initializeConnection() {
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
