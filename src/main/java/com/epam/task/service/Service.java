package com.epam.task.service;

import com.epam.task.dao.implementation.ApplicationDaoImpl;
import com.epam.task.dao.implementation.BusDaoImpl;
import com.epam.task.dao.implementation.UserDaoImpl;
import com.epam.task.model.Application;
import com.epam.task.model.Bus;
import com.epam.task.model.User;

import java.sql.SQLException;

public class Service {
    public void create(User user) throws SQLException{
        new UserDaoImpl().create(user);
    }

    public void create(Bus bus) throws SQLException{
        new BusDaoImpl().create(bus);
    }

    public void create(Application application) throws SQLException{
        new ApplicationDaoImpl().create(application);
    }

    public void submitApplication(int driver_id, int bus_id, int application_id) throws SQLException{
        new ApplicationDaoImpl().submitApplication(driver_id, bus_id, application_id);
    }
}
