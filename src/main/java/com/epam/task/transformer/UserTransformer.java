package com.epam.task.transformer;

import com.epam.task.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserTransformer {

    public User fromResultSetToObject(ResultSet rs) throws SQLException {
        User user = null;
        if (rs.next()) {
            user = new User();
            user.setUser_type_id(rs.getInt("user_type_id"));
        user.setFirst_name(rs.getString("first_name"));
        user.setLast_name(rs.getString("last_name"));
        user.setUser_name(rs.getString("user_name"));
        user.setPassword(rs.getString("password"));
        user.setUser_type_id(rs.getInt("user_type_id"));
    }
        return user;
    }
}
