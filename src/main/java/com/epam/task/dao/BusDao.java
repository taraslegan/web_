package com.epam.task.dao;

import com.epam.task.model.Bus;

import java.sql.SQLException;

public interface BusDao {
    void create(Bus bus) throws SQLException;
}
