package com.epam.task.dao;

import com.epam.task.model.User;

import java.sql.SQLException;

public interface UserDao {
    void create(User user) throws SQLException;
//    int getIdByUserName(String user_name) throws SQLException;
}
