package com.epam.task.dao;

import com.epam.task.model.Application;

import java.sql.SQLException;

public interface ApplicationDao {
    void create(Application application) throws SQLException;
    void submitApplication(int driver_id, int bus_id, int application_id) throws SQLException;
}
