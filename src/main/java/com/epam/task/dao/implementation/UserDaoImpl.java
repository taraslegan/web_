package com.epam.task.dao.implementation;

import com.epam.task.dao.UserDao;
import com.epam.task.model.User;
import com.epam.task.persistant.ConnectionManager;

import java.sql.*;

public class UserDaoImpl implements UserDao {

    private final String CREATE_USER = "INSERT INTO users(first_name, last_name, user_name, password, user_type_id) " +
            "values(?, ?, ?, ?, ?);";
    private final String CREATE_CLIENT = "INSERT INTO clients(id, phone, email) " +
            "values(?, ?, ?);";
    private final String CREATE_DRIVER = "INSERT INTO drivers(id, phone, email, country, city, photo) " +
            "values(?, ?, ?, ?, ?, ?);";

    public void create(User user) throws SQLException {
        int id;
        Connection connection = ConnectionManager.getConnection();
        switch (user.getUser_type_id()) {
            case 1:
                id = createUser(connection, user);
                createClient(connection, user, id);
                break;
            case 2:
                createUser(connection, user);
                break;
            case 3:
                id = createUser(connection, user);
                createDriver(connection, user, id);
                break;
        }
    }

    private int createUser(Connection connection, User user) throws SQLException {
        int key;
        try (PreparedStatement ps = connection.prepareStatement(CREATE_USER, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getFirst_name());
            ps.setString(2, user.getLast_name());
            ps.setString(3, user.getUser_name());
            ps.setString(4, user.getPassword());
            ps.setInt(5, user.getUser_type_id());

            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            key = keys.getInt(1);
        }
        return key;
    }


    private void createClient(Connection connection, User user, int id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE_CLIENT)) {
            ps.setInt(1, id);
            ps.setString(2, user.getPhone());
            ps.setString(3, user.getEmail());

            ps.executeUpdate();
        }
    }

    private void createDriver(Connection connection, User user, int id) throws SQLException {
        try(PreparedStatement ps = connection.prepareStatement(CREATE_DRIVER)) {
            ps.setInt(1, id);
            ps.setString(2, user.getPhone());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getCountry());
            ps.setString(5, user.getCity());
            ps.setString(6, user.getPhoto());

            ps.executeUpdate();
        }
    }
}