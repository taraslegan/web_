package com.epam.task.dao.implementation;

import com.epam.task.dao.ApplicationDao;
import com.epam.task.model.Application;
import com.epam.task.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ApplicationDaoImpl implements ApplicationDao {
    private final String CREATE_APPLICATION = "INSERT INTO applications(client_id, country, city_from, city_to, " +
            "bus_capacity) values(?, ?, ?, ?, ?);";
    private final String UPDATE_APPLICATION = "UPDATE applications SET driver_id = ?, bus_id = ? WHERE id = ?;";
    @Override
    public void create(Application application) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE_APPLICATION)) {
            ps.setInt(1, application.getClient_id());
            ps.setString(2, application.getCountry());
            ps.setString(3, application.getCity_from());
            ps.setString(4, application.getCity_to());
            ps.setInt(5, application.getBus_capacity());

            ps.executeUpdate();
        }
    }

    @Override
    public void submitApplication(int driver_id, int bus_id, int application_id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE_APPLICATION)) {
            ps.setInt(1, driver_id);
            ps.setInt(2, bus_id);
            ps.setInt(3, application_id);

            ps.executeUpdate();
        }
    }
}
