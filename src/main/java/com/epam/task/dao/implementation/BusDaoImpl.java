package com.epam.task.dao.implementation;

import com.epam.task.dao.BusDao;
import com.epam.task.model.Bus;
import com.epam.task.model.enums.Conditioner;
import com.epam.task.persistant.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BusDaoImpl implements BusDao {

    private final String CREATE_BUS = "INSERT INTO buses(model, conditioner, tv, toilet, tables," +
            "comfortable_seats, extra_trunk, fridge, capacity, photo) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    public void create(Bus bus) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE_BUS)) {
            ps.setString(1, bus.getModel());
            ps.setString(2, bus.getConditioner().toString());
            ps.setString(3, bus.getTv().toString());
            ps.setString(4, bus.getToilet().toString());
            ps.setString(5, bus.getTables().toString());
            ps.setString(6, bus.getComfortable_seats().toString());
            ps.setString(7, bus.getExtra_trunk().toString());
            ps.setString(8, bus.getFridge().toString());
            ps.setInt(9, bus.getCapacity());
            ps.setString(10, bus.getPhoto());

            ps.executeUpdate();
        }
    }
}
