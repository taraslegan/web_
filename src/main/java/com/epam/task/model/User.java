package com.epam.task.model;

public class User {
    private String first_name;
    private String last_name;
    private String user_name;
    private String password;
    private Integer user_type_id;
    private String phone;
    private String email;
    private String country;
    private String city;
    private String photo;

    public User() {
    }

    public User(String first_name,
                String last_name,
                String user_name,
                String password,
                Integer user_type_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.user_name = user_name;
        this.password = password;
        this.user_type_id = user_type_id;
    }

    public User(String first_name,
                String last_name,
                String user_name,
                String password,
                Integer user_type_id,
                String phone,
                String email) {
        this(first_name, last_name, user_name, password, user_type_id);
        this.phone = phone;
        this.email = email;
    }

    public User(String first_name,
                String last_name,
                String user_name,
                String password,
                Integer user_type_id,
                String phone,
                String email,
                String country,
                String city,
                String photo) {
        this(first_name, last_name, user_name, password, user_type_id, phone, email);
        this.country = country;
        this.city = city;
        this.photo = photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUser_type_id() {
        return user_type_id;
    }

    public void setUser_type_id(Integer user_type_id) {
        this.user_type_id = user_type_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
