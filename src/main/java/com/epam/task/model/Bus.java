package com.epam.task.model;

import com.epam.task.model.enums.*;

public class Bus {
    private String model;
    private Conditioner conditioner;
    private Tv tv;
    private Toilet toilet;
    private Tables tables;
    private ComfortableSeats comfortable_seats;
    private ExtraTrunk extra_trunk;
    private Fridge fridge;
    private Integer capacity;
    private String photo;

    public Bus() {
    }

    public Bus(String model,
               Conditioner conditioner,
               Tv tv, Toilet toilet,
               Tables tables, ComfortableSeats comfortable_seats,
               ExtraTrunk extra_trunk,
               Fridge fridge,
               Integer capacity,
               String photo) {
        this.model = model;
        this.conditioner = conditioner;
        this.tv = tv;
        this.toilet = toilet;
        this.tables = tables;
        this.comfortable_seats = comfortable_seats;
        this.extra_trunk = extra_trunk;
        this.fridge = fridge;
        this.capacity = capacity;
        this.photo = photo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Conditioner getConditioner() {
        return conditioner;
    }

    public void setConditioner(Conditioner conditioner) {
        this.conditioner = conditioner;
    }

    public Tv getTv() {
        return tv;
    }

    public void setTv(Tv tv) {
        this.tv = tv;
    }

    public Toilet getToilet() {
        return toilet;
    }

    public void setToilet(Toilet toilet) {
        this.toilet = toilet;
    }

    public Tables getTables() {
        return tables;
    }

    public void setTables(Tables tables) {
        this.tables = tables;
    }

    public ComfortableSeats getComfortable_seats() {
        return comfortable_seats;
    }

    public void setComfortable_seats(ComfortableSeats comfortable_seats) {
        this.comfortable_seats = comfortable_seats;
    }

    public ExtraTrunk getExtra_trunk() {
        return extra_trunk;
    }

    public void setExtra_trunk(ExtraTrunk extra_trunk) {
        this.extra_trunk = extra_trunk;
    }

    public Fridge getFridge() {
        return fridge;
    }

    public void setFridge(Fridge fridge) {
        this.fridge = fridge;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
