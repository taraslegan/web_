package com.epam.task.model;

public class Application {
    private Integer client_id;
    private String country;
    private String city_from;
    private String city_to;
    private Integer bus_capacity;
    private Integer driver_id;
    private Integer bus_id;

    public Application() {}

    public Application(Integer client_id, String country, String city_from, String city_to, Integer bus_capacity) {
        this.client_id = client_id;
        this.country = country;
        this.city_from = city_from;
        this.city_to = city_to;
        this.bus_capacity = bus_capacity;
    }

    public Integer getClient_id() {
        return client_id;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity_from() {
        return city_from;
    }

    public void setCity_from(String city_from) {
        this.city_from = city_from;
    }

    public String getCity_to() {
        return city_to;
    }

    public void setCity_to(String city_to) {
        this.city_to = city_to;
    }

    public Integer getBus_capacity() {
        return bus_capacity;
    }

    public void setBus_capacity(Integer bus_capacity) {
        this.bus_capacity = bus_capacity;
    }

    public Integer getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(Integer driver_id) {
        this.driver_id = driver_id;
    }

    public Integer getBus_id() {
        return bus_id;
    }

    public void setBus_id(Integer bus_id) {
        this.bus_id = bus_id;
    }
}
