package com.epam.task;

import com.epam.task.model.Application;
import com.epam.task.model.Bus;
import com.epam.task.model.User;
import com.epam.task.model.enums.*;
import com.epam.task.service.Service;

import java.sql.SQLException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws SQLException {
        Service service = new Service();

//        Bus bus = new Bus("Sprinter", Conditioner.NO, Tv.NO, Toilet.NO, Tables.NO, ComfortableSeats.YES,
//                ExtraTrunk.YES, Fridge.NO, 30,"path to photo");
//        service.create(bus);

//        Application application =
//                new Application(
//                        2,
//                        "Ukraine",
//                        "Lviv",
//                        "Kyiv",
//                        50);
//        service.create(application);
        service.submitApplication(3, 1, 3);

    }
}
