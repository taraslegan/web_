create database autobase;
use autobase;

create table user_type(
id integer primary key auto_increment,
role varchar(50)
);

select * from user_type; 
select * from users; 
alter table users auto_increment = 1;
delete from users;

create table users(
id integer primary key auto_increment, 
first_name varchar(50),
last_name varchar(50),
user_name varchar(50),
password varchar(50),
user_type_id integer, foreign key(user_type_id) references user_type(id)
);

create table clients(
id integer primary key, foreign key(id) references users(id),
phone varchar(50),
email varchar(50)
);

drop table clients;
select * from clients;

create table drivers(
id integer primary key, foreign key(id) references users(id),
phone varchar(50),
email varchar(50),
country varchar(50),
city varchar(50),
photo text,
bus_id integer, foreign key(bus_id) references buses(id)
);
select * from drivers;

create table buses(
id integer primary key auto_increment,
model varchar(100),
conditioner enum('YES', 'NO'),
tv enum('YES', 'NO'),
toilet enum('YES', 'NO'),
tables enum('YES', 'NO'),
comfortable_seats enum('YES', 'NO'),
extra_trunk enum('YES', 'NO'),
fridge enum('YES', 'NO'),
capacity integer,
photo text
);


drop table buses;
select * from buses;

create table applications(
id integer primary key auto_increment,
client_id integer, foreign key(client_id) references clients(id),
country varchar(50),
city_from varchar(50),
city_to varchar(50),
bus_capacity integer,
driver_id integer, foreign key(driver_id) references drivers(id),
bus_id integer, foreign key(bus_id) references buses(id)
);
insert into applications(client_id, country, city_from, city_to, bus_capacity) 
values(2,"asdf","asdf","asdf",20);

select *  from applications;
drop table applications;
update applications set 
driver_id = 3, bus_id = 1 where id = 2;